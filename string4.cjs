const full_Name = (obj) =>
{
    const fin_Arr = [];     //Array to store the name
    let fin_String =''; //String to store the final name
    for(let key in obj)
    {
        fin_Arr.push(obj[key].toLowerCase().trim());
    }
    for(let index =0;index<fin_Arr.length;index++)
    {
        let string_1 = fin_Arr[index].charAt(0);        //Extracting the first element
        let string_2 = string_1.toUpperCase();          //converting the first element to Uppercase for Titlecase output

        fin_String += (fin_Arr[index].replace(string_1 , string_2))+' ';    //storing the name after replacing the first letters
    }
    return fin_String; 
};

module.exports = full_Name;