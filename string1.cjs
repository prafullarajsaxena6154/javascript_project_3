const convert_type = (input) =>
{
    let result = "";
    result = input.replace(/\$/,'');
    
    result = result.replace(/,/,'');
    
    let num = Number(result);
    
    if(Number.isNaN(num))
    {
        return [];   
    }
    else 
    {
        return num;
    }
    
};
module.exports = convert_type ;