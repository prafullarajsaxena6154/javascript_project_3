const ip_Address_Array = (input) => 
{
    input.trim();   //removing white spaces from both ends
    input+='.';     //adding '.' for further processing of code
    let fin_Array = []; //final array to contain final output if valid
    let temp_String = "";
    for(let index = 0 ; index < input.length ; index++)
    {
        
        let arr_Element = null;
        if(input.charCodeAt(index) > 47 && input.charCodeAt(index) < 58)
        {
            temp_String += input.charAt(index);     //stores numeric values characters
        }
        else if(input.charCodeAt(index) === 46)     //checks if '.' encountered
        {
            arr_Element = Number(temp_String);
            if(arr_Element>=0 && arr_Element<256)    //checking for the highest number in every 8 bit part
            {
                temp_String = "";
                fin_Array.push(arr_Element);
            }
            else 
            {
                return [];
            }
        }
        else
        {
            return [];                         //returns empty array when characters other than numbers and . are encountered
        }
    }
    if(fin_Array.length === 4)                 //returns the final array only when the IP address has 4 parts
    {
        return fin_Array;
    }
    return [];
};

module.exports = ip_Address_Array;