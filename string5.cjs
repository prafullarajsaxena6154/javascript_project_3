const arr_To_String = (obj) =>
{
    let fin_String = '';
    if(obj !== null && obj !== undefined)       //checking if the object is defined
    {
        for (let index in obj)
        {
            fin_String+=obj[index]+' ';         //adding to string to be returned
        }
    }
    return fin_String;
};
module.exports = arr_To_String;