const find_Month = (input) => 
{
    let temp_Arr = input.split('/',3); //Splitting and creating an array of 3 elements

    for(let index = 0 ; index < 3; index++)
    {
        if(temp_Arr.length !== 3)       //to check if there are atleast 3 values in date
        {
            return undefined;
        }
        else 
        {
            if(temp_Arr[index] !== undefined && temp_Arr[0] !== '') //to check if the elements are defined
            {
                if(!(/\D/.test(temp_Arr[index])))       //to check if the elements don't contain any characters other than numbers
                {
                    continue;
                }
                else 
                {
                    return undefined;
                }
            }
            else
            {
                return undefined;
            }
        }
    }

    let month_Num = (Number(temp_Arr[1]))-1;

    let mon_Arr = ['January','February','March','April','May','June','July','August','September','October','November','December'];

    return mon_Arr[month_Num];
    
};    
module.exports = find_Month;
// console.log(find_Month('2023/12/2'));